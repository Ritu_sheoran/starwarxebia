function Planets(state = {}, action) {
    console.log("action", action)
    switch (action.type) {
        case "SAVE_PLANETS_MAP":
            return {...state, ...action.value}
        case "SAVE_PLANETS":
            return {...state, ...action.planets};
        default:
            return {...state};
    }
}

export default Planets;
