import {reactLocalStorage} from 'reactjs-localstorage';

export function getUserData(user){
    return {type:'save_user',user:user}
}
export function fetchUserData(user, successCallback, failureCallback){
    return (dispatch) =>{
        let options = {
            method: "GET",
            headers:{
                'Accept':'application/json',
                'Content-Type':'application/json'
            }
        }
        fetch("https://swapi.co/api/people/1/", options).then(response =>{
            if(!response.ok){
                throw Error(response.statusText);
            }
            return response;
        }).then(res =>res.json())
            .then(actualUser => {
                console.log(actualUser)
                if(user.username == actualUser.name && user.password == actualUser.birth_year){
                    console.log("user logged in");
                    successCallback();
                } else {
                    failureCallback();
                }
            }).catch(err => err)
    }

}