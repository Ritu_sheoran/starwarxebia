function savePlanets(planets){
    return {type:'SAVE_PLANETS',planets: {...planets}}
}

function savePlanetsMap(planets) {
    return {type: "SAVE_PLANETS_MAP",planetsMap: {...planets}}
}
export function searchPlanet(searchText, successCallback, failureCallback){
    return (dispatch, getState) =>{
        var planetsMap = getState();
        let options = {
            method: "GET",
            headers:{
                'Accept':'application/json',
                'Content-Type':'application/json'
            }
        }
        var url = `https://swapi.co/api/planets/`;
        if(searchText) {
            url += `?search=${searchText}`
        }
        fetch(url, options).then(response =>{
            if(!response.ok){
                throw Error(response.statusText);
            }
            return response;
        }).then(res =>res.json())
            .then(planets => {
                dispatch(savePlanets(planets));
            }).catch(err => err)
    }

}

export function addMorePlanets(url, success, failure) {
    return (dispatch, getState) => {
        var olderPlanets = getState().Planets;
        fetch(url, {
            accept: "application/json"
        })
            .then((res) => res.json())
            .catch(error => console.log("error", error))
            .then(planets => {
                if(planets && planets.results) {
                }

                    // var results = [...planets.results];
                    if(planets && planets.results) {
                        olderPlanets.next = planets.next;
                        var planets = [...planets.results];
                        olderPlanets.results.push(...planets);
                        dispatch(savePlanets(planets));
                        // success();
                    } else {
                        // failure();
                    }
                    // dispatch(savePlanets(planets));
                }
            )
    }
}

//fetch the planets to show on list
export function getPlanets(){
    console.log("getPlanets called");
    return (dispatch) => {
        fetch("https://swapi.co/api/planets/", {
            accept: "application/json"
        })
            .then((res) => res.json())
            .catch(error => console.log("error", error))
            .then(planets => {
                    dispatch(savePlanets(planets));
                }
            )
    }
}
