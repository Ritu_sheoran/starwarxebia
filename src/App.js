import React, { Component } from 'react';
import Login from"./components/login/Login";
import Search from"./components/Search";
import logo from './logo.svg';
import './App.css';
import {
    Route,
    Switch
} from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Switch>
          <Route exact path="/" component={Login} />
          <Route path="/search" component={Search} />
        </Switch>
      </div>
    );
  }
}

export default App;
