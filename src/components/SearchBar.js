import React from "react";

function SearchBar(props) {
    return(
        <div>
            <input className="search-text" placeholder="Search" type="text" onChange={props.searchText}/>
        </div>
    )
}

export default SearchBar;