import React,{Component} from 'react';
import "./Planet.css";

const PlanetDetails = (props) => {
    // console.log("props detail>>>>>>>",JSON.stringify(props));
    return(
        <div style={props.index % 2 !== 0 ? {backgroundColor: "whitesmoke"}: {}}>
            <div className="App-listvalue"> {props.planet.population}</div>
            <div className="App-listvalue">{props.planet.name}</div>
            <div className="App-listvalue">{props.planet.surface_water}</div>
            <div className="App-listvalue">{props.planet.orbital_period}</div>
            <div className="App-listvalue">{props.planet.diameter}</div>
        </div>
    )
}
export default PlanetDetails;