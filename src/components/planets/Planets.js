import React, {Component} from 'react';
import PlanetDetails from './PlanetDetail';
import {connect} from 'react-redux';
import {getPlanets, addMorePlanets} from '../../redux/Actions/search';
import "./Planet.css";
import InfiniteScroll from 'react-infinite-scroller';

class PlanetList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasMore: true
        }
        this.loadMore = this.loadMore.bind(this);
    }
    componentDidMount() {
        this.props.dispatch(getPlanets());
    }

    loadMore(page) {
       if(this.props.Planets.next) {
           this.props.dispatch(addMorePlanets(this.props.Planets.next))
       }
    }

    render() {
        if (this.props && this.props.Planets && this.props.Planets.results && this.props.Planets.results.length > 0) {
            var planetArray = this.props.Planets.results;
            var PlanetData = planetArray.map((planet, index) => {
                console.log("enter for planet call");
                return (
                    <PlanetDetails
                        key={index}
                        index={index}
                        planet={planet}
                    />
                );
            });
        }

        return (<InfiniteScroll
            pageStart={0}
            loadMore={this.loadMore}
            hasMore={this.state.hasMore}>
                <div className="App-list">
                    <div className="App-listTitle">
                        <div className="App-listHeader">Population</div>
                        <div className="App-listHeader">Name</div>
                        <div className="App-listHeader">Surface Water</div>
                        <div className="App-listHeader">Orbital Period</div>
                        <div className="App-listHeader">Diameter</div>
                    </div>
                    <div className="App-value">
                        {PlanetData}
                    </div>
                </div>
            </InfiniteScroll>

        )
    }
}

var mapStateToProps = (state) => {
    return state;
}


PlanetList = connect(mapStateToProps)(PlanetList);

export default PlanetList;