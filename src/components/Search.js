import React from "react";
import {connect} from "react-redux";
import { withRouter } from "react-router-dom";
import SearchBar from './SearchBar';
import Planets from './planets/Planets';
import {searchPlanet} from '../redux/Actions/search';
import debounce from 'lodash/debounce';

class Search extends React.Component{
    constructor(props) {
        super(props);
        this.searchText = this.searchText.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(searchPlanet())
    }

    searchText= debounce((text) => {
        this.props.dispatch(searchPlanet(text));
    },1000);

    render(){
        return (
            <div>
                <SearchBar searchText={(e)=>{console.log(e);this.searchText(e.target.value)}}/>
                <Planets />
            </div>
        )
    }
}

Search =connect()(Search)
export default withRouter(Search);