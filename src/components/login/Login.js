import React from "react";
import {connect} from "react-redux";
import {fetchUserData,getUserData} from "../../redux/Actions/login";
import { withRouter } from "react-router-dom";
import './login.css';

class Login extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            error: ""
        }
        this.loginSuccess = this.loginSuccess.bind(this);
        this.loginFailure = this.loginFailure.bind(this);
    }
    submitLoginForm(e){
        e.preventDefault();

        var user = {
            username: this.refs.username.value,
            password: this.refs.password.value
        };

        this.props.dispatch(fetchUserData(user, this.loginSuccess, this.loginFailure));
    }

    loginSuccess() {
        this.props.history.push("/search");
    }

    loginFailure() {
        this.setState({error: "Username or Password is incorrect."})
    }

    render(){
       return (
           <div className="login-form">
               <form onSubmit={this.submitLoginForm.bind(this)}>
                   <label htmlFor="username">Username</label><br/>
                   <input id="username" type="text" placeholder="Enter Username" defaultValue="Luke Skywalker" ref="username"/>
                   <br/>
                   <label htmlFor="password" className="label">Password</label><br/>
                   <input id="password" type="password" placeholder="Enter Password" defaultValue={"19BBY"} ref="password"/>
                   <br/>
                   <span>{this.state.error}</span>
                   <div className="submit-button">
                       <input type="submit" value="Log In"/>
                   </div>
               </form>
           </div>
       )
    }
}

this.loginFailure

Login =connect()(Login)
export default withRouter(Login);