import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from"react-redux";
import Reducers from "./redux/Reducers/index"
import {createStore,combineReducers,applyMiddleware} from "redux";
import thunk from 'redux-thunk';
import {
    BrowserRouter as Router
} from "react-router-dom";

let store = createStore(combineReducers(Reducers),applyMiddleware(thunk))
console.log("store", store);
ReactDOM.render(
    <Provider store={store}>
        <Router>
            <App />
        </Router>
    </Provider>, document.getElementById('root'));
registerServiceWorker();
